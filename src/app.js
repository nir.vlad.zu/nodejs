const express = require('express')
const fs = require('fs/promises')
const path = require('path')

const app = express()
app.use(express.json())
const PORT = 8080
const dir = path.join(__dirname, 'content')

function getFileExtension(fileName) {
  return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length)
}

const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js']
function isAllowedExtension(extension) {
  return allowedExtensions.includes(extension)
}

async function getFiles() {
  try {
    return fs.readdir(dir)
  } catch (err) {
    console.error('Error occurred while reading directory! ' + err)
  }
}
async function createFile(fileName, content) {
  try {
    await fs.writeFile(path.join(dir, fileName), content)
  } catch (err) {
    console.error('Whoops something went wrong ' + err)
  }
}
app.get('/api/files', async (req, res) => {
  try {
    const files = await getFiles()
    res.status(200).json({
      message: 'Success',
      files: files,
    })
    console.log(`/GET a list of files: ${files}`)
  } catch (err) {
    console.error(`Server error`)
    return res.status(500).json({
      message: 'Server error',
    })
  }
})

app.post('/api/files', async (req, res) => {
  try {
    if (req.body.filename === undefined || req.body.content === undefined) {
      console.error(`/POST ${req.body} with status code 400, Bad request`)
      return res.status(400).json({
        message: 'Bad request',
      })
    }
    if (!isAllowedExtension(getFileExtension(req.body.filename))) {
      console.error(`/POST ${req.body} with status code 400, Not allowed extension`)
      return res.status(400).json({
        message: 'Not allowed extension',
      })
    }
    const createdFiles = await getFiles()
    console.log(createdFiles)
    if (createdFiles.includes(req.body.filename)) {
      console.error(`/POST ${req.body} with status code 400, File exists`)
      return res.status(400).json({
        message: 'File exists',
      })
    }
    await createFile(req.body.filename, req.body.content)
    console.log(`/POST with status code 200, File created with name = ${req.body.filename}`)
    return res.status(200).json({
      message: 'File created successfully',
    })
  } catch (err) {
    console.error(`Server error`)
    return res.status(500).json({
      message: 'Server error',
    })
  }
})
async function readFile(fileName) {
  const data = await fs.readFile(path.join(dir, fileName), 'utf8')
  return data.toString()
}
app.get('/api/files/:filename', async (req, res) => {
  try {
    const { filename: fileName } = req.params
    if (!(await getFiles()).includes(fileName)) {
      console.error(`No file with '${fileName}' filename found`)
      return res.status(400).json({
        message: `No file with '${fileName}' filename found`,
      })
    }
    res.status(200).json({
      message: 'Success',
      filename: `${fileName}`,
      content: await readFile(fileName),
      extension: getFileExtension(fileName),
      uploadedAt: (await fs.stat(path.join(dir, fileName))).birthtime,
    })
    console.log(`/GET:${fileName} with a status code 200`)
  } catch (err) {
    res.status(500).json({
      message: 'Server error',
    })
  }
})
app.listen(PORT)
